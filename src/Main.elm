module Main exposing (main, toString)

import Browser
import Element exposing (..)
import Element.Background as Background
import Element.Font as Font
import Element.Input exposing (button)
import Html exposing (Html)
import Maybe



--- MODEL ---


type alias Model =
    { count : Int }


initModel : Model
initModel =
    { count = 0 }



--- UPDATE ---


type Msg
    = Increment
    | Decrement


update : Msg -> Model -> Model
update msg model =
    case msg of
        Increment ->
            { model | count = model.count + 1 }

        Decrement ->
            { model | count = model.count - 1 }



--- VIEW ---


{-| Convert a number to string (this is an example to display the
elm-verify-example functionality)

    toString 5 --> "5"

-}
toString : Int -> String
toString n =
    String.fromInt n


smallButton : String -> Msg -> Element Msg
smallButton text msg =
    button
        [ Background.color (rgba 0 0 0 1)
        , Font.color (rgba 1 1 1 1)
        , Font.italic
        , Font.size 32
        ]
        { onPress = Just msg, label = Element.text text }


view : Model -> Html Msg
view model =
    row [ centerX, centerY ]
        [ smallButton "-" Decrement
        , Element.text <| toString model.count
        , smallButton "+" Increment
        ]
        |> Element.layout []



--- PROGRAM ---


main : Program () Model Msg
main =
    Browser.sandbox
        { init = initModel
        , update = update
        , view = view
        }
