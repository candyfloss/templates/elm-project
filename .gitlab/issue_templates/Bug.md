## Summary

<!-- Summarize the bug encountered concisely -->

## Steps to reproduce

<!-- How one can reproduce the issue - this is important -->

1. 
2. 
3. 

## What is the current bug behavior?

<!-- What actually happens -->

## What is the expected correct behavior?

<!-- What you should see instead -->

## Relevant logs, screen shots or recordings

<!-- Paste any relevant logs - please use code blocks (```) to format console output, logs, and code as it's hard to read otherwise. -->
<!-- Share screen shots if possible, they help immensely in understanding your problem. -->
<!-- If you prefer to record a GIF, there's a set of tools that can help: 

- [Gifox](https://gifox.io) - **$4.99** - Cleanest UI, hotkeys, lots of advanced features
- [Giphy Capture](https://giphy.com/apps/giphycapture) - **FREE** - Easy to upload to giphy.com, slightly annoying UX.
- [LICEcap](https://www.cockos.com/licecap/) - **FREE** - Less intuitive, more features
- [Recordit](http://recordit.co/) - **FREE** - Simple, clean UI, but auto-uploads to [recordit.co](http://recordit.co)
- [ttystudio](https://github.com/chjj/ttystudio) - **FREE** - For commandline tools, a terminal-to-gif recorder minus the headaches.

-->

## Possible fixes

<!-- If you can, link to the line of code that might be responsible for the problem -->

## Your Environment

<!--- Include relevant details about the environment you use -->
<!--- Example:
- Browser vendor and version (e.g.: Google Chrome 71.0.3578.98):
- Operating System and version (e.g.: Windows 7):
-->

/label ~bug ~needs-investigation
/cc @project-manager
/assign @qa-tester
