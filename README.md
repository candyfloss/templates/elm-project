![Gitlab template for Elm projects](https://cl.ly/4c83b075cf1d/gitlab-icon-1-color-black-rgb-eps(3).png)

# project-template

> Gitlab template for Elm projects

When you are creating a new project, you can pick this project as the base
template, by taking advantage of Gitlab's [custom project templates][].

## Libraries

- [elm-ui][]
- [create-elm-app][]

### elm-ui


### create-elm-app

This project is compatible with [create-elm-app][], so is possible to use the
[elm-app][] commands provided.

You should probably familiarize yourself with the [documentation][].

[elm-ui]: https://github.com/mdgriffith/elm-ui
[create-elm-app]: https://github.com/halfzebra/create-elm-app
[elm-app]: https://github.com/halfzebra/create-elm-app/blob/master/template/README.md#available-scripts
[documentation]: https://github.com/halfzebra/create-elm-app/blob/master/template/README.md
[custom project templates]: https://docs.gitlab.com/ee/user/group/custom_project_templates.html